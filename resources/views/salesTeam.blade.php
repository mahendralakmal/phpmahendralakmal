@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            Team
            <a href="{{ route('sales_teams.create') }}" class="btn btn-primary float-right">Add New</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">email</th>
                    <th scope="col">Telephone</th>
                    <th scope="col">Current Route</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($teams as $key => $team)
                    <tr>
                        <th scope="row">{{ $key+1 }}</th>
                        <td>{{ $team->fullName }}</td>
                        <td>{{ $team->email }}</td>
                        <td>{{ $team->telephone }}</td>
                        <td>{{ $team->currentRoute }}</td>
                        <td>
                            <a href="{{ route('sales_teams.show', $team->id) }}" class="text-info">View</a> |
                            <a href="{{ route('sales_teams.edit', $team->id) }}" class="text-warning">Edit</a> |
                            <a href="{{ route('sales_teams.destroy', $team->id) }}" class="text-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pull-right">{{ $teams->links() }}</div>
        </div>
    </div>

@endsection
