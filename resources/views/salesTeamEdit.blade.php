@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            Add New Sales Representative
        </div>
        <div class="card-body">
            <form action="{{ route('sales_teams.update', $salesTeam->id) }}" method="put" id="createMember" enctype="application/x-www-form-urlencoded">
                @csrf
                <input type="hidden" id="id" name="id" value="{{ $salesTeam->id }}">
                <div class="form-group">
                    <label for="fullName">Full Name</label>
                    <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name" value="{{ $salesTeam->fullName }}">
                </div>
                <div class="form-group">
                    <label for="email">Email Address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{ $salesTeam->email }}">
                </div>
                <div class="form-group">
                    <label for="tele">Telephone</label>
                    <input type="text" class="form-control" id="tele" name="tele" placeholder="Telephone" max="15" value="{{ $salesTeam->telephone }}">
                </div>
                <div class="form-group">
                    <label for="dateOfJoined">Joined Date</label>
                    <input type="date" class="form-control" id="dateOfJoined" name="dateOfJoined" value="{{ $salesTeam->dateOfJoined }}">
                </div>
                <div class="form-group">
                    <label for="currentRoute">Current Route</label>
                    <select class="form-control" id="currentRoute" name="currentRoute">
                        @foreach($routes as $key=>$route)
                            <option value="{{ $route->id }}" @if($salesTeam->currentRoute == $route->id) selected @endif>{{ $route->route }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="comment">Comment</label>
                    <textarea class="form-control" id="comment" name="comment" rows="3" >{{ $salesTeam->comment }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
