<?php

namespace App\Http\Controllers;

use App\SalesTeamRoute;
use Illuminate\Http\Request;

class SalesTeamRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesTeamRoute  $salesTeamRoute
     * @return \Illuminate\Http\Response
     */
    public function show(SalesTeamRoute $salesTeamRoute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesTeamRoute  $salesTeamRoute
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesTeamRoute $salesTeamRoute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesTeamRoute  $salesTeamRoute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesTeamRoute $salesTeamRoute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesTeamRoute  $salesTeamRoute
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesTeamRoute $salesTeamRoute)
    {
        //
    }
}
