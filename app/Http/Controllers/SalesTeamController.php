<?php

namespace App\Http\Controllers;

use App\SalesTeam;
use App\SalesTeamRoute;
use Illuminate\Http\Request;

class SalesTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $teams = SalesTeam::withoutTrashed()->paginate(5);
        return view('salesTeam', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $routes = SalesTeamRoute::withoutTrashed()->get();
        return view('salesTeamAdd', compact('routes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $res = $request->validate([
            'fullName' => 'required',
            'email' => 'required',
            'telephone' => 'required',
            'dateOfJoined' => 'required|date',
            'currentRoute' => 'required',
        ]);
        $team = new SalesTeam();

        $team->fullName = $request->fullName;
        $team->email = $request->email;
        $team->telephone = $request->telephone;
        $team->dateOfJoined = $request->dateOfJoined;
        $team->currentRoute = $request->currentRoute;
        $team->comment = $request->comment;
        $res = $team->save();

        return redirect(route('sales_teams.index'))->with('success', 'Team Member Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesTeam  $salesTeam
     * @return \Illuminate\Http\Response
     */
    public function show(SalesTeam $salesTeam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesTeam  $salesTeam
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(SalesTeam $salesTeam)
    {
        $routes = SalesTeamRoute::withoutTrashed()->get();
        return view('salesTeamEdit',compact('salesTeam', 'routes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesTeam  $salesTeam
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, SalesTeam $salesTeam)
    {
        $res = $request->validate([
            'fullName' => 'required',
            'email' => 'required',
            'telephone' => 'required',
            'dateOfJoined' => 'required|date',
            'currentRoute' => 'required',
        ]);


        $salesTeam->fullName = $request->fullName;
        $salesTeam->email = $request->email;
        $salesTeam->telephone = $request->telephone;
        $salesTeam->dateOfJoined = $request->dateOfJoined;
        $salesTeam->currentRoute = $request->currentRoute;
        $salesTeam->comment = $request->comment;
        $res = $salesTeam->update();

        return redirect(route('sales_teams.index'))->with('success', 'Team Member Added Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesTeam  $salesTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesTeam $salesTeam)
    {
        //
    }
}
