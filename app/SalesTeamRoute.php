<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesTeamRoute extends Model
{
    use SoftDeletes;

    public function salesTeam(){
        return $this->hasMany('App\SalesTeam');
    }
}
