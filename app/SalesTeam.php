<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesTeam extends Model
{
    use SoftDeletes;

    public function salesTeamRoute(){
        return $this->belongsTo('App\SalesTeamRoute');
    }
}
